// Dependencies
require('should');
const sinon = require('sinon');

const userController = require('../controllers/userController');

describe('User Controller Test', () => {
  describe('Post', () => {
    it('should not allow empty email on Post', () => {
      /**
       * This would create a fake User that takes user as param and also contain
       * fake save method which does nothing.
       * @param {UserSchema} user
       * @constructor
       */
      const User = function(user) {
        this.save = () => {
        };
      };

      /**
       * Mocking request which has a fake body, That contains
       * firstname property.
       * @type {{body: {firstName: string}}}
       */
      const req = {
        body: {
          firstName: 'John',
        },
      };

      /**
       * Using sinon spy to records arguments,
       * return value, the value of this and exception
       * @type {{json, send, status}}
       */
      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };

      /**
       * Invoking controller.post() with mocked req and res
       * @type {*|{post: function(*, *): *, get: get}}
       */
      const controller = userController(User);
      controller.post(req, res);

      /**
       * Assert getting bad status because
       * we are sending just a first name for user.
       */
      res.status.calledWith(400).
          should.
          equal(true, `Bad status ${res.status.args[0][0]}`);

      /**
       * Also assert that controller should send
       * "Email is required" as response.
       */
      res.send.calledWith('Email is required').should.equal(true);
    });
  });
});
