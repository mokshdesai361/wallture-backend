const request = require('supertest');
process.env.NODE_ENV = 'Development';
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app).get('/').expect(/Welcome to Wallture Backend/, done);
  });
});
