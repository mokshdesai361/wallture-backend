/**
 * Using should to assert various things.
 * @type {should | ((obj: any) => should.Assertion)}
 */
const should = require('should');

/**
 * Using SuperTest to test our API end points
 */
const request = require('supertest');

/**
 * Using Mongoose to create User model and invoke various methods needs to
 * be used in the testcase.
 * @type {module:mongoose}
 */
const mongoose = require('mongoose');

/**
 * Using faker to generate fake data for pushing into database.
 * @type {Faker}
 */
const faker = require('faker');

/**
 * Indication to use Test environment,
 * So Mongoose will connect to Test database instead of Production.
 * @type {string}
 */
process.env.NODE_ENV = 'Development';

/**
 * Since Supertest needs express reference to work with,
 * We need to import it.
 * @type {*|Express|{}}
 */
const app = require('../app.js');

/**
 * Using mongoose to create user model(Schema)
 * @type {Model<Document>}
 */
const User = mongoose.model('User');

/**
 * Initialize supertest agent from app.
 * @type {TestAgent|*}
 */
const agent = request.agent(app);

describe('User CRUD Integration Test', () => {
  it('should allow User to be Posted and Return data with _id', (done) => {
    /**
     * Using faker to generate fake random data.
     */
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email();

    /**
     * Creating a dummy user to insert to database.
     * @type {{firstName: *, lastName: *, email: *}}
     */
    const user = {
      'email': email,
      'firstName': firstName,
      'lastName': lastName,
    };

    /**
     * Using supertest agent to send POST request with dummy user.
     */
    agent.post('/users').send(user)
        // Then assert to expect 201
        .expect(201)
        // Then check multiple things,
        .end((error, result) => {
          // There should not be an error
          should.not.exist(error);
          // And there should be an id for resulted object.
          result.body.should.have.property('_id');
          // And finally invoke done() to indicate we are done with this test.
          done();
        });
  });

  /**
   * This runs after *each* test is complete, Regardless of it's result.
   */
  afterEach((done) => {
    // We delete every entry in Test database after this test is done.
    User.deleteMany({}).exec();
    done();
  });

  /**
   * This runs after *whole* test is done. Regardless of result.
   */
  after(((done) => {
    /**
     * We close mongoose connection since we are done
     * with everything at this point.
     */
    mongoose.connection.close();
    done();
  }));
});
