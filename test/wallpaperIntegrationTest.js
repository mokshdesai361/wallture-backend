/**
 * Using should to assert various things.
 * @type {should | ((obj: any) => should.Assertion)}
 */
const should = require('should');

/**
 * Using SuperTest to test our API end points
 */
const request = require('supertest');

/**
 * Using Mongoose to create User model and invoke various methods needs to
 * be used in the testcase.
 * @type {module:mongoose}
 */
const mongoose = require('mongoose');

/**
 * Using faker to generate fake data for pushing into database.
 * @type {Faker}
 */
const faker = require('faker');

/**
 * Indication to use Test environment,
 * So Mongoose will connect to Test database instead of Production.
 * @type {string}
 */
process.env.NODE_ENV = 'Development';

/**
 * Since Supertest needs express reference to work with,
 * We need to import it.
 * @type {*|Express|{}}
 */
const app = require('../app.js');

/**
 * Using mongoose to create user model(Schema)
 * @type {Model<Document>}
 */
const Wallpaper = mongoose.model('Wallpapers');

/**
 * Initialize supertest agent from app.
 *  @type {Test|*}
 */
const agent = request(app);

/**
 * Wallpaper CRUD Integration test to check proper
 * functioning of Routers and Controllers.
 */
describe('Wallpaper CRUD Integration Test', () => {
  // Test for the POST request.
  describe('POST', () => {
    it('should allow Wallpaper to be Posted and Return data with _id',
        (done) => {
          // Creating a dummy wallpaper
          const wallpaper = {
            name: 'Sample Wallpaper',
            tags: [
              'test',
              'debug',
            ],
            category: [
              'sample',
            ],
            copyleft: '',
            url: faker.image.imageUrl(),
            author: {
              authorName: faker.name.firstName(),
              authorUrl: 'https://example.com/',
            },
            likes: 69420,
            downloads: 42069,
            isAmoled: false,
            isAnime: false,
          };

          // Sending the poster via POST, should get a 201
          agent.post('/wallpapers').
              send(wallpaper).
              expect(201).
              end((error, result) => {
                should.not.exist(error);
                result.body.should.have.property('_id');
                done();
              });
        });
  });

  // Test for the GET Request
  describe('GET', (done) => {
    it('should allow to query for wallpapers.', (done) => {
      // Querying without parameter to get all wallpapers.
      agent.get('/wallpapers').then((response) => {
        response.status.should.be.equal(200);
        done();
      });
    });
  });

  /**
   * This runs after *whole* test is done. Regardless of result.
   */

  after(((done) => {
    // We close every connection mongoose connection pool.
    Wallpaper.deleteMany({}).exec();
    mongoose.disconnect();
    done();
  }));
});


