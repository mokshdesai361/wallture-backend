module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
  },
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 11,
    'es2021': true,
  },
  'rules': {
  },
};
