// Dependencies
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const wallpaperModel = require('../models/wallpaperModel');
const wallpaperController = require('../controllers/wallpaperController');
const controller = wallpaperController(wallpaperModel);

// GET Requests -> To be passed to wallpaperController.get()
router.get('/', controller.get);

// POST Requests -> To be passed to wallpaperController.post()
router.post('/', controller.post);

// Middleware to response to specific queries
router.use('/:wallpaperId', (req, res, next) => {
  // Find user from wallpaperId found in request.
  wallpaperModel.findById(req.params.wallpaperId, (error, wallpaper) => {
    if (error) {
      // if there is a generic error throw 503
      return res.status(503).send(error);
    }
    // if wallpaper is found, Put it into request object.
    if (wallpaper) {
      req.wallpaper = wallpaper;
      return next();
    }
    // this is the last exit point, if everything above is not the case send 404
    return res.sendStatus(404);
  });
});

// Return wallpaper using wallpaperID
router.get('/:wallpaperId', (req, res) => {
  // Parsing the result into JSON
  const returnWallpaper = req.wallpaper.toJSON();
  // Initializing links with blank json object.
  returnWallpaper.links = {};
  returnWallpaper.links.FilterByTheName = `http://${req.headers.host}/wallpapers/?name=${req.wallpaper.name}`;
  res.json(returnWallpaper);
});

module.exports = router;
