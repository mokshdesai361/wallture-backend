const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const User = require('../models/userModel');

const MongoCastError = require('../utils/MongoCastError');
const PatchWithID = require('../utils/PatchWithID');

const userController = require('../controllers/userController');
const controller = userController(User);

/**
 * Gets user
 */
router.get('/', controller.get);

/**
 * Posts user.
 */
router.post('/', controller.post);

/**
 * Middleware to get user whenever there is userId present in GET request
 */
router.use('/:userId', (req, res, next) => {
  // Find user from userId found in request.
  User.findById(req.params.userId, (error, user) => {
    if (error) {
      if (error.name === 'CastError') {
        // If there is a casting error throw custom error
        return next(
            new MongoCastError(`Cast for ${req.params.userId} unsuccessful.`),
        );
      }
      // if there is a generic error throw 503
      return res.status(503).send(error);
    }
    // if user is found, Put it into request object.
    if (user) {
      req.user = user;
      return next();
    }
    // this is the last exit point, if everything above is not the case send 404
    return res.sendStatus(404);
  });
});

/**
 * Gets user with userID passed in request.
 */
router.get('/:userId', (req, res) => {
  /**
   * Optional modification for user object which can be removed
   * but added for demonstration.
   */
  const returnUser = req.user.toJSON();
  returnUser.links = {};
  returnUser.links.FilterByThisFirstName =
      `http://${req.headers.host}/users/?firstName=${req.user.firstName}`;
  res.json(returnUser);
});

/**
 * put callback for User.save() method
 * @param {Response} res
 * @param {User} user
 * @return {function(*=): *}
 */
function putUserCallback(res, user) {
  return (err) => {
    if (err) res.send(err);
    return res.status(200).json(user);
  };
}

/**
 * Puts user passed in request body
 */
router.put('/:userId', (req, res) => {
  const {user} = req;
  user.email = req.body.email;
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.passwordHash = req.body.passwordHash;
  user.favoriteWallpapers = req.body.favoriteWallpapers;
  req.user.save(putUserCallback(res, user));
});

/**
 * Patch callback for User.save() method
 * @param {Response} res
 * @param {User} user
 * @return {function(*=): *}
 */
function patchUserCallback(res, user) {
  return (err) => {
    if (err) res.send(err);
    return res.status(200).json(user);
  };
}

router.patch('/:userId', (req, res) => {
  const {user} = req;
  // Do not accept ID in patch request.
  if (req.body._id) throw new PatchWithID();
  // Modify each entry found with new value sent in patch request.
  Object.entries(req.body).forEach((it) => {
    const key = it[0];
    user[key] = it[1];
  });
  // Then finally save and return the object if patch is successful
  req.user.save(patchUserCallback(res, user));
});

router.delete('/:userId', (req, res) => {
  // Get the user from request and invoke remove().
  req.user.remove((error) => {
    if (error) return res.send(error);
    // If request was successful, Send 204 as response.
    return res.sendStatus(204);
  });
});

module.exports = router;
