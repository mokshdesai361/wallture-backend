const mongoose = require('mongoose');

const {Schema} = mongoose;

const userModel = new Schema(
    {
      email: {type: String, required: true},
      firstName: {type: String, required: true},
      lastName: {type: String, required: true},
      passwordHash: {type: String, default: null},
      favoriteWallpapers: [String],
    },
);

module.exports = mongoose.model('User', userModel);
