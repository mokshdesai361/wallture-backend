/**
 * A custom error wrapper class to wrap custom errors in application.
 * It is a base class which will be extended in other custom error
 * classes.
 */
class WalltureError extends Error {
  /**
   * Constructor for the custom error class which would
   * instantiate the Error with values provided.
   * @param {String} message Error message.
   * @param {Number} status Error status code.
   */
  constructor(message, status) {
    super();

    /**
     * Capturing stacktrace for WalltureError
     */
    Error.captureStackTrace(this, this.constructor);

    /**
     * Get class name when a custom error which extends WalltureError
     * is thrown. It is not being used anywhere now but added it for future
     * needs in optimization of error page.
     * @type {string}
     */
    this.name = this.constructor.name;

    /**
     * Message for error. If no error message is supplied
     * it takes default input.
     * @type {string}
     */
    this.message = message || 'Something went wrong. Please try again.';

    /**
     * Status code for error. If no status code was supplied in custom error
     * error class definition, It sets status 500 to be thrown in error.
     * @type {number}
     */
    this.status = status || 500;
  }
}

/**
 * Export the class to be used in individual error classes as base class.
 * @type {WalltureError}
 */
module.exports = WalltureError;
