const WalltureError = require('./WalltureError');

/**
 * Custom error which extends WalltureError class.
 * It is responsible to throw a custom error when a patch request contains
 * _id in request body.
 */
class PatchWithID extends WalltureError {
  /**
   * Constructor which takes message for the error.
   * @param {String} message
   */
  constructor(message) {
    super(message || 'Can\'t patch when ID is present in body.', 406);
  }
}

module.exports = PatchWithID;
