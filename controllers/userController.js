/**
 * Controller for Users route which is responsible to facilitate
 * GET and POST request for /users path.
 * @param {UserSchema} User
 * @return {{post: (function(*, *): *), get: get}}
 */
function userController(User) {
  /**
   * Post method responsible to do POST operations on data received in request.
   * @param {Request} req
   * @param {Response} res
   * @return {*}
   */
  function post(req, res) {
    const user = new User(req.body);
    /**
     * Check whether body contains email.
     * If not, send status code 400 and return.
     */
    if (!req.body.email) {
      res.status(400);
      return res.send('Email is required');
    }
    user.save();
    res.status(201);
    return res.json(user);
  }

  /**
   * Returns modified user object with self link in it.
   * @param {UserSchema} users
   * @param {Request} req
   * @return {*}
   */
  function getReturnUsers(users, req) {
    return users.map((user) => {
      const newUser = user.toJSON();
      newUser.links = {};
      newUser.links.self = `http://${req.headers.host}/users/${user._id}`;
      return newUser;
    });
  }

  /**
   * Get method responsible to do GET operation for user router.
   * @param {Request} req Request object from Http request
   * @param {Response} res Response object from Http request
   */
  function get(req, res) {
    const query = {};
    if (req.query.email) {
      query.email = req.query.email;
    }
    if (req.query.firstName) {
      query.firstName = req.query.firstName;
    }
    if (req.query.lastName) {
      query.lastName = req.query.lastName;
    }
    // Gets all user, Find method has no params or query for now.
    User.find(query, (err, users) => {
      // Send error if fetching has been failed
      if (err) return res.send(err);
      // If user collection has no data, execute this
      if (users.length <= 0) return res.json({message: 'No users found'});
      // If everything goes according to plan, Send users data in JSON format
      return res.status(200).json(getReturnUsers(users, req));
    });
  }

  return {post, get};
}

module.exports = userController;
