// Dependencies area
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

// API Routers (endpoints)
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const wallpapersRouter = require('./routes/wallpapers');

/**
 * Initialize Express
 * @type {*|Express}
 */
const app = express();

// Running async task to check connection with mongodb
run().catch((error) => {
  console.error(error.stack);
});

/**
 * This function checks whether it can connect to mongodb with mongoose.
 * if not, It exists the application with exit code 1.
 * Because without database connection, There is no point to keep application
 * running.
 * @return {Promise<void>}
 */
async function run() {
  console.log(`Mongoose Version: ${mongoose.version}`);

  // Current date to calculate how much time it took to throw an error
  const start = Date.now();
  // Options for MongoDB connection.
  const opts = {useNewUrlParser: true, useUnifiedTopology: true};

  // Configuration to determine connection to Prod or Test databse.
  let connectionString = null;
  // If ENV is set to Test, Lets connect to test database
  if (process.env.NODE_ENV === 'Development') {
    console.log('Connecting to Test Database!');
    connectionString = 'mongodb://localhost/wallture_Test';
  } else {
    // Else, Proceed with production database.
    console.log('Connecting to Production Database!');
    connectionString = 'mongodb://localhost/wallture-prod';
  }
  // Asynchronous mongodb connection attempt that would either success or fail.
  await mongoose.connect(connectionString, opts).
      catch((error) => {
        // Log the indecent to console.
        console.log(`Caught "${error.message}" after ${Date.now() - start}`);
        // Exit process a.k.a. kill the API server
        process.exit(1);
      });
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Setup morgan on Dev level
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Index route which serves the Welcome page.
app.use('/', indexRouter);

// Users route, Serving CRUD for user.
app.use('/users', usersRouter);
app.use('/wallpapers', wallpapersRouter);

app.all('*', (req, res, next) => {
  res.status(404).json({
    status: 'fail',
    message: `Can't find ${req.originalUrl} on this server!`,
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);

  // render the error page
  res.render('error');
});

// Listeners for database connections.
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  /* We are Connected.
   * But there is no logging statement to keep test cases output sane.
   */
});

/**
 * Export app.
 * @type {*|Express}
 */
module.exports = app;
